dbs = [
    "admin",
    "rapidcargo",
]

dbs.forEach(dbName => {
    db = new Mongo().getDB(dbName);

    if (dbName === 'admin') {
        db.createUser({
            user: "rapidcargo",
            pwd: "P4ssw0rd33",
            roles: [
                { role: "clusterMonitor", db: dbName}
            ]
        })
    } else {
        db.createUser({
            user: "rapidcargo",
            pwd: "P4ssw0rd33",
            roles: [
                { role: "readWrite", db: dbName },
                { role: "dbOwner", db: dbName },
            ]
        })
    }
})
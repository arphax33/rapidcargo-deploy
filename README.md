# RapidCargo Deploy

## Pre-requisite

* Having a running **Docker engine** (WSL or Desktop)

## How to deploy in local

* Clone all the repositories in the same folder
  * **[rapidcargo-front](https://source.cyclonic.fr/lil.berna/rapidcargo-front)** ([backup link](https://gitlab.com/arphax33/rapidcargo-front))
  * **[rapidcargo-back](https://source.cyclonic.fr/lil.berna/rapidcargo-back)** ([backup link](https://gitlab.com/arphax33/rapidcargo-back))
  * **[rapidcargo-deploy](https://source.cyclonic.fr/lil.berna/rapidcargo-deploy)** ([backup link](https://gitlab.com/arphax33/rapidcargo-deploy))
* Go in **rapidcargo-deploy/**
  * Run `docker-compose up -d --build`
  * Access your local environment on http://localhost

## Troubleshooting

### `docker-compose up -d --build` is stuck or do not work

* Go in **rapidcargo-back/**
  * Run `docker build . -f ./.auto/Dockerfile -t rapidcargo-back`
* Go in **rapidcargo-front/**
  * Run `docker build . -f ./.auto/Dockerfile -t rapidcargo-front`
* Go in **rapidcargo-deploy/**
  * Run `docker-compose up -d`
* Access your local environment on http://localhost
